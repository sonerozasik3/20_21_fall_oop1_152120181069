#include <stdio.h>

void update(int *a, int *b) {
	int plus, minus;
	plus = (*a) + (*b);
	minus = (*a) - (*b);
	if (minus < 0)
		minus = minus * (-1);
	*a = plus;
	*b = minus;
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}
