#include<iostream>
#include<stdio.h>
using namespace std;
void printOutput(int*,int);
int sumOfNumbers(int*, int);
int productNumbers(int*, int);
int Control(FILE*);
int smallestNumber(int*, int);
float avarage(int, int);
int main() {
	// Opening the input file ("input.txt").
	FILE *stream;
	errno_t err = fopen_s(&stream, "input.txt", "r");
	int n=-1 ,sum,product;
	int *Numbers;// Numbers Array that memorized input numbers.
	if (err == 0)
	{}
	else
	{
		printf("ERROR! The file was not opened.\n");
		system("pause");
		return 0;
	}
	//Reading numbers from input file.
	fscanf_s(stream, "%d", &n);
	if (n > 0) {
		if (Control(stream)) {
			int empty;
			rewind(stream);
			fscanf_s(stream, "%d", &n);
			Numbers = (int*)malloc(n * sizeof(int));
			for (int i = 0; i < n; i++) {
				fscanf_s(stream, "%d", &Numbers[i]);
			}
			printOutput(Numbers,n);
		}
		else
			cout << "Invalid Input!" << endl;
	}
	else
		cout << "Invalid Input!"<<endl;
	system("pause");
}
//!Function that prints output. @param *A Array which memorize the input numbers @param n Count of input numbers
void printOutput(int *A,int n) {
	int sum, product, smallest;
	float avg;
	sum = sumOfNumbers(A, n);
	product = productNumbers(A, n);
	avg=avarage(sum, n);
	smallest=smallestNumber(A, n);
	cout << "Sum is " << sum << endl;
	cout << "Product is " << product << endl;
	cout << "Avarage is " << avg << endl;
	cout << "Smallest is " << smallest << endl<<endl;
}
//!Function does summation operation. @param *A Array which memorize the input numbers @param n Count of input numbers
int sumOfNumbers(int *A, int n) {
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum = sum + A[i];
	}
	return sum;
}
//!Function does multipication operation. @param *A Array which memorize the input numbers @param n Count of input numbers
int productNumbers(int*A, int n) {
	int product = 1;
	for (int i = 0; i < n; i++) {
		product *= A[i];
	}
	return product;
}
//!Controls the input file is it correct. @param *stream Input file
int Control(FILE*stream) {
	int empty,counter=0;
	char c;
	while (1) {
		c = fgetc(stream);
		if (feof(stream)) {
			break;
		}
		if (48 > (int)c ||(int) c > 57) {
			if (c == '\n' || c == ' '){}
			else {
				return 0;
			}
		}
	}
	int n;
	char c1, c2;
	rewind(stream);
	fscanf_s(stream, "%d", &n);
	while (!feof(stream)) {
		c1 = fgetc(stream);
		if (48 <= (int)c1 && (int)c1<=57) {
			counter++;
			if (48 <= (int)c2 && (int)c2 <= 57) {
				counter--;
			}
		}
		c2 = c1;
	}
	if (counter == n) 
		return 1;
	else
		return 0;
}
//!Function finds the smallest number of array. @param *A Array which memorize the input numbers @param n Count of input numbers
int smallestNumber(int*A, int n) {
	int small;
	small = A[0];
	for (int i = 1; i < n; i++) {
		if (A[i] < small)
			small = A[i];
	}
	return small;
}
//!Function calculates avarage of array. @param sum Total of the input numbers @param n Count of input numbers
float avarage(int sum, int n) {
	return sum / n;
}