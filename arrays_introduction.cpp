#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	int n, *A;
	cin >> n;
	A = (int*)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++) {
		cin >> A[i];
	}
	for (int i = n - 1; i >= 0; i--) {
		cout << A[i] << " ";
	}
	return 0;
}

