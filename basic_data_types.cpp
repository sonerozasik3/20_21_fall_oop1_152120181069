#include <iostream>
#include <cstdio>
using namespace std;

int main() {
	int i;
	long int li;
	char ch;
	float f;
	double d;
	scanf("%d %ld %c %f %lf", &i, &li, &ch, &f, &d);
	printf("%d\n", i);
	printf("%ld\n", li);
	printf("%c\n", ch);
	printf("%.3f\n", f);
	printf("%.9lf\n", d);
	return 0;
}