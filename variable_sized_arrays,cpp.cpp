#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	int n, **A, q;
	cin >> n;
	cin >> q;
	A = (int**)malloc(sizeof(int*)*n);
	for (int i = 0; i < n; i++) {
		int x;
		cin >> x;
		A[i] = (int*)malloc(sizeof(int)*x);
		for (int j = 0; j < x; j++) {
			cin >> A[i][j];
		}
	}
	for (int i = 0; i < q; i++) {
		int x, y;
		cin >> x;
		cin >> y;
		cout << A[x][y] << endl;
	}
	return 0;
}
